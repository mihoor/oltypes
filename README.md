# README #

### What is this repository for? ###

The repository holds OLTypes Delphi library.  
The library enhances basic types with some useful methods and ability to store Null values.  
It was tested with Delphi 2010, Delphi XE and Delphi 10.1. It will not work in Delphi 2005 and below.

###Licence###
OLTypes library is distributed under **[the MIT License](https://www.wikiwand.com/en/MIT_License)**.

### How do I get set up? ###

* Download library and unpack it.
* Add all units to your project or add path to source folder to your project's Search path (Menu Project->Options->Delphi Compiler->Search path->Browser for folder->Add->OK)
* Add OLTypes to the uses section in your program

When you add source folder to Library path  
(Menu Tools->Options->Environment Options->Delphi Options->Library->Library path [...]->Browser for folder->Add->OK)  
then every new project will have OLTypes in its Browsing path.

**You may also want to open the the OLTypesTest.dpr to run the tests and see the new types in use before you start using it yourself.**

### Contribution guidelines ###

Please write me when you

* found a bug,
* have a suggestion,
* wrote an interesting modification.

 Repo owner and admin: **[rajewicz+olt@gmail.com](mailto:rajewicz+olt@gmail.com)**